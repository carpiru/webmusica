<?php  
class Coleccion{
	//Me creo las propiedades de la Classe
	public $discos;
	public $titulo;

	//Me creo los metodos de la Classe
	public function __construct($titulo=''){
		$this->discos=[]; //Le digo que discos, es un vector
		$this->titulo=$titulo;
	}

	//Me creo un metodo para agregar discos
	public function agregar($disco){
		$this->discos[]=$disco;
	}

	//Me creo un metodo para listar los discos
	public function listar(){
		return $this->discos;
	}

}


?>