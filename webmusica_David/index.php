<?php  

require('classes/disco.class.php');
require('classes/coleccion.class.php');


//ANTES DE NADA, VOY A COMPROBAR SI EL USUARIO HA RELLENADO UN NUEVO
//CONTACTO, Y SI ES ASI, LO INSERTO EN EL FICHERO "datos.txt"

//Parto de la opcion que NO inserto un disco
$discoInsertado=false;

if(isset($_POST['enviar'])){

  //Marco de alguna forma, que VOY a insertar un disco
  $discoInsertado=true;

  //Partimos de que TODOS los datos son correctos
  $datosCorrectos=true;
  $datosMensaje='';
  $tipoMensaje='danger';

  //Recojo el resto de datos del formulario
  $nombre=$_POST['nombre'];
  if(strlen($nombre)<2){
    $datosCorrectos=false;
    $datosMensaje.='El nombre del disco no es correcto<br>';
  }

  $año=$_POST['año'];
  if( (strlen($año)!=4) OR (!is_numeric($año)) ){
    $datosCorrectos=false;
    $datosMensaje.='El año debe ser un numero de 4 digitos<br>';
  }

  $grupo=$_POST['grupo'];
  if(strlen($grupo)<2){
    $datosCorrectos=false;
    $datosMensaje.='El nombre del grupo no es correcto<br>';
  }

  $canciones=$_POST['canciones'];
  if( !is_numeric($canciones) ) {
    $datosCorrectos=false;
    $datosMensaje.='El numero de canciones no es valido<br>';
  }

  //Hay que poner el fichero en su sitio
  if(!is_uploaded_file($_FILES['portada']['tmp_name'])){
    $datosCorrectos=false;
    $datosMensaje.='Debes elegir una portada para el disco<br>';
  }

  if($datosCorrectos==true){

    $datosMensaje='Disco insertado con exito<br>';
    $tipoMensaje='success';

    move_uploaded_file($_FILES['portada']['tmp_name'], 'imagenes/'.$_FILES['portada']['name']);
  
    //Creo la linea que insertare en el archivo de texto
    $linea="\r\n".$nombre.';'.$año.';'.$grupo.';'.$canciones.';'.$_FILES['portada']['name'];

    //Abro el fichero en modo escritura
    $fichero=fopen('datos.txt','a'); //Para agregar al final

    //Escribo mi linea en el fichero
    fwrite($fichero, $linea);

    //Cierro el fichero
    fclose($fichero);

  }else{

    echo 'Debes elegir una imagen';

  }


}

//Me creo un objeto de la clase coleccion
$coleccion=new Coleccion('Coleccion de discos');

//Quiero rellenar los contactos, a partir de un fichero de texto
//Asi vemos como leer y escribir en un fichero de texto
//Vamos a ver funciones para manejar ficheros de texto
//Abrir el fichero
$fichero=fopen('datos.txt','r'); //Modo read

//Leer el fichero, linea a linea
while($linea=fgets($fichero)){
  $partes=explode(';',$linea);
  $nombre=trim($partes[0]);
  $año=trim($partes[1]);
  $grupo=trim($partes[2]);
  $canciones=trim($partes[3]);
  $portada=trim($partes[4]); //Quita espacios en blanco
  $coleccion->agregar(new Disco($nombre, $año, $grupo, $canciones, $portada));
}

//Cierro el fichero
fclose($fichero);

?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Gestion de discos con ficheros de texto</title>
 
    <!-- CSS de Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="css/bootstrap-superhero.min.css" rel="stylesheet" media="screen">
  </head>
  <body>
    
    <section class="container">
      
      <!-- ENCABEZADO DE LA WEB -->
      <header>
        <h2>
          <a href="index.php">
          Gestion de discos 
          <small><?php echo $coleccion->titulo; ?></small>
          </a>
        </h2>
        <hr>
      </header>

      <!-- EN EL CASO DE INSERTAR UN NUEVO  DISCO, MUESTRO UNA VENTANA -->
      <?php if($discoInsertado==true){ ?>
        <div class="alert alert-<?php echo $tipoMensaje;?>">
          <?php echo $datosMensaje;?>
        </div>
      <?php } ?>

      <!-- SECCION CENTRAL DE LA WEB -->
      <section class="row">

        <!-- COLUMNA IZQUIERDA -->
        <div class="col-md-8">
          <table border="1" class="table table-hover table-striped">
            <tr>
              <th>Nombre</th>
              <th>Año</th>
              <th>Grupo</th>
              <th>Canciones</th>
              <th>Portada</th>
            </tr>
            <?php foreach($coleccion->listar() as $disco){ ?>
              <tr>
                <td><?php echo $disco->nombre; ?></td>
                <td><?php echo $disco->año; ?></td>
                <td><?php echo $disco->grupo; ?></td>
                <td><?php echo $disco->canciones; ?></td>
                <td>
                  <img src="imagenes/<?php echo $disco->portada; ?>" width="100">
                </td>
              </tr>
            <?php } ?>
          </table>
        </div>

        <!-- COLUMNA DERECHA -->
        <div class="col-md-4">
         <form action="index.php" method="post" enctype="multipart/form-data"> 

            <input type="text" class="form-control" name="nombre" placeholder="Escribe el nombre del disco"><br>
            <input type="text" class="form-control" name="año" placeholder="Escribe el año del disco"><br>

            <input type="text" class="form-control" name="grupo" placeholder="Escribe el nombre del grupo"><br>

            <input type="text" class="form-control" name="canciones" placeholder="Escribe el numero de canciones"><br>

            <input type="file" class="form-control" name="portada"><br>

            <input type="submit" name="enviar" value="Enviar"><br>
          </form>
        </div>

      </section>

      <!-- PIE DE PAGINA DE LA WEB -->
      <footer>
        <hr>
        Todos los derechos reservados &copy;
      </footer>


    </section>

    <!-- Librería jQuery requerida por los plugins de JavaScript -->
    <script src="js/jquery-3.3.1.min.js"></script>
 
    <!-- Todos los plugins JavaScript de Bootstrap -->
    <script src="js/bootstrap.min.js"></script>

  </body>
</html>