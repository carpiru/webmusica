<?php  

require('classes/disco.class.php');
require('classes/coleccion.class.php');


//ANTES DE NADA, VOY A COMPROBAR SI EL USUARIO HA RELLENADO UN NUEVO
//CONTACTO, Y SI ES ASI, LO INSERTO EN EL FICHERO "datos.txt"
if(isset($_POST['enviar'])){
  //Recojo el resto de datos del formulario
  $nombre=$_POST['nombre'];
  $año=$_POST['año'];
  $grupo=$_POST['grupo'];
  $canciones=$_POST['canciones'];

  // Hay que poner el fichero (lña imagen) en su carpeta
  // Para ello, mueve (move...) desde... a...
  // move_uploaded_file($_FILES['portada']['tmp_name'], 'imagenes/'.$_FILES['portada']['name']);

  // Para que no de error si no se sube archivo:
  if (is_uploaded_file($_FILES['portada']['tmp_name'])){
  move_uploaded_file($_FILES['portada']['tmp_name'], 'imagenes/'.$_FILES['portada']['name']);
  }




  $canciones=$_POST['canciones'];

  //Creo la linea que insertare en el archivo de texto
  $linea="\r\n".$nombre.';'.$año.';'.$grupo. ';' .$canciones. ';'
.$_FILES['portada']['name'];

  //Abro el fichero en modo escritura
  $fichero=fopen('datos.txt','a'); //Para agregar al final



  //Escribo mi linea en el fichero
  fwrite($fichero, $linea);
  

  //Cierro el fichero
  fclose($fichero);

}

//Me creo un objeto de la clase coleccion
$coleccion=new Coleccion('Coleccion de discos');

//Quiero rellenar los contactos, a partir de un fichero de texto
//Asi vemos como leer y escribir en un fichero de texto
//Vamos a ver funciones para manejar ficheros de texto
//Abrir el fichero
$fichero=fopen('datos.txt','r'); //Modo read

//Leer el fichero, linea a linea
while($linea=fgets($fichero)){
  $partes=explode(';',$linea);
  $nombre=trim($partes[0]);
  $año=trim($partes[1]);
  $grupo=trim($partes[2]);
  $canciones=trim($partes[3]);
  $portada=trim($partes[4]);
  $coleccion->agregar(new Disco($nombre, $año, $grupo, $canciones, $portada)); //constructor
}

//Cierro el fichero
fclose($fichero);

?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Plantilla básica de Bootstrap</title>
 
    <!-- CSS de Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet" media="screen">
  </head>
  <body>
    
    <section class="container">
      
      <!-- ENCABEZADO DE LA WEB -->
      <header>
        <h2>
          <a href="index.php">
          Gestion de discos 
          <small><?php echo $coleccion->titulo; ?></small>
          </a>
        </h2>
        <hr>
      </header>

      <!-- SECCION CENTRAL DE LA WEB -->
      <section class="row">

        <!-- COLUMNA IZQUIERDA -->
        <div class="col-md-8">
          <table border="1" class="table table-hover table-striped">
            <tr>
              <th>Nombre</th>
              <th>Año</th>
              <th>Grupo</th>
              <th>Nº canciones</th>
              <th>Portada</th>
            </tr>
            <?php foreach($coleccion->listar() as $disco){ ?>
              <tr>
                <td><?php echo $disco->nombre; ?></td>
                <td><?php echo $disco->año; ?></td>
                <td><?php echo $disco->grupo; ?></td>
                <td><?php echo $disco->canciones; ?></td>
                <td>
                  <!-- agregar la portada -->
                  <img src="imagenes/<?php echo $disco->portada; ?>" width="100">
                </td>
              </tr>
            <?php } ?>
          </table>
        </div>

        <!-- COLUMNA DERECHA -->
        <div class="col-md-4">
          <!-- enctype="multipart/form-data" es para que guarde ficheros, eb este caso imagenes -->
         <form action="index.php" method="post" enctype="multipart/form-data"> 
            <input type="text" class="form-control" name="nombre" placeholder="Escribe el nombre del grupo"><br>
            <input type="text" class="form-control" name="año" placeholder="Escribe el año del grupo"><br>
            <input type="text" class="form-control" name="grupo" placeholder="Escribe el nombre del grupo"><br>
            <input type="text" class="form-control" name="canciones" placeholder="Escribe el numero de canciones"><br>
            <!-- Para incorporar imagen en el formulario -->
            <input type="file" class="form-control" name="portada"> <br>
            <input type="submit" name="enviar" value="Enviar"><br>
          </form>
        </div>

      </section>

      <!-- PIE DE PAGINA DE LA WEB -->
      <footer>
        <hr>
        Todos los derechos reservados &copy;
      </footer>


    </section>

    <!-- Librería jQuery requerida por los plugins de JavaScript -->
    <script src="js/jquery-3.3.1.min.js"></script>
 
    <!-- Todos los plugins JavaScript de Bootstrap -->
    <script src="js/bootstrap.min.js"></script>

  </body>
</html>